export const dataGeneralSettings =  [
    {
        "name": "TARGET_MONTHLY",
        "value": "500000"
    },
    {
        "name": "NOTIFICATIONS_EMAILS",
        "value": "mail@mail.com"
    },
    {
        "name": "ACTIVE_SEND_EMAIL",
        "value": "false"
    },
    {
        "name": "ACTIVE_SEND_SMS",
        "value": "false"
    }, 
    {
        "name" : "DEFAULT_LANG",
        "value" : "ar"
    },
    {
        "name" : "DEFAULT_THEM",
        "value" : "light"
    }
];