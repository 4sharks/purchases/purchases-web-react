import InvForm from "../../components/invoice/add/InvForm";
import withGuard from "../../utils/withGuard";


const CreateInvoice = ({companySelected,branchSelected,tenantId,tenantUsername}) => {

    return (
        <div>
            <InvForm
                tenantId = {tenantId}
                tenantUsername ={tenantUsername}
                companySelected = {companySelected}
                branchSelected = {branchSelected}
            />
        </div>
    )
}

export default withGuard(CreateInvoice)