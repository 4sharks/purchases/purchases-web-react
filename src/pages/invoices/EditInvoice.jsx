import { useParams } from "react-router-dom";
import InvFormUpdate from "../../components/invoice/edit/InvFormUpdate";
import { useCurrent, useFetch } from "../../hooks";
import withGuard from "../../utils/withGuard";
import { MwSpinner } from "../../components/ui";


const EditInvoice = () => {
    const {currentTenantId,currentCompanyId,currentBranchId} = useCurrent();
    const params = useParams();
    const invID = params.id;
    const {data,loading,error} = useFetch(`${process.env.REACT_APP_INV_API_SERVER_BASE_URL}/invoices/show/${params.id}`)
    if(!invID) return ;


    return (
        <div className="  w-full h-full">
            {!loading && !error && data ? <InvFormUpdate
                tenantId = {currentTenantId}
                tenantUsername ={params.tenant}
                companySelected = {currentCompanyId}
                branchSelected = {currentBranchId}
                invData = {data}
            /> : <MwSpinner className='flex  items-center justify-center'/>
            }
        </div>
    )
}

export default withGuard(EditInvoice)