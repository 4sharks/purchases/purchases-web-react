import React from 'react'
import {AiOutlineCopyrightCircle} from 'react-icons/ai'
import { Link } from 'react-router-dom'
const Footer = () => {
    return (
        <div className="fixed bottom-1 w-full  h-7 flex justify-between py-2 px-5  text-xs text-slate-500  ">
            <div className='mx-12'>Version <span className='font-bold'>{process.env.REACT_APP_VERSION}</span></div>
            <div className='flex justify-center items-center gap-1 '> 
                <span>ALL CopyRight </span> 
                <AiOutlineCopyrightCircle/>
                <span> <span><Link target='_blank' to={`https://4sharks.org`}>4SHARKS</Link> </span>FOR SOFTWARE SOLUTIONS</span>
            </div>
        </div>
    )
}

export default Footer