import React from 'react'
import {  useTranslation } from 'react-i18next';

const InvoiceBody = ({
    customerName,
    customer,
    salesmanName,
    invProducts,
    isDelivery,
    SETTING_INV_CURRENCY,
    SETTING_INV_VAT_PERCENT,
    formatter
}) => {
    console.log(' customerName',customerName,customer);

    const [t] = useTranslation('global')
    const invProductsList = invProducts.map((prod) => (
        <div key={prod.id} className={`flex justify-between border-b  ${ prod.is_returned && 'bg-orange-50'}`}>
            <div className='flex-1 p-2 border-e'>{prod.product_name} {prod.is_returned && <span className='font-bold text-orange-300'>[مرتجع]</span> } </div>
            {!isDelivery && <div className='w-20 text-center  border-e p-2'>{formatter.format(prod.price).slice(0,-6)} </div>}
            <div className='w-16 text-center border-e p-2'>{prod.qty}</div>
            {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.total_price).slice(0,-6)} </div>}
            {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.product_discount).slice(0,-6)} </div>}
            {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.product_net_total).slice(0,-6)} </div>}
            {/* {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.product_vat).slice(0,-6)} </div>} */}
            {!isDelivery && <div className='w-24 text-center border-e p-2'>{formatter.format(prod.product_net_total_with_vat).slice(0,-6)} </div>}
        </div>
    ));

    return (
        <>
            <div className='text-xs '>
                <div className='flex justify-between text-xs  pb-3  '>
                    {customer && customerName && 
                        <div className='border flex-1 flex items-center justify-between gap-3 p-2 '> 
                        <div className='flex flex-col text-right'>
                            <span>اسم المورد</span>
                            <span>Vendor NAME</span>
                        </div>
                            <span className='font-bold'>{customer?.fullname[0].text || customer?.fullname[1].text }</span>
                        </div>
                    }
                    {customer?.vendorNo && 
                        <div className='border flex-1 flex items-center justify-between gap-3 p-2 '>
                            <div className='flex flex-col text-right'>
                                <span>رقم المورد</span>    
                                <span >Vendor NO</span>
                            </div>
                            <span className='font-bold'>{customer.vendorNo}</span>
                        </div>
                    }
                    {customer?.vendorAddresses && 
                        <div className='border flex-1 flex items-center justify-between gap-3 p-2 '>
                            <div className='flex flex-col text-right'>
                                <span>عنوان المورد</span>    
                                <span >Vendor ADDRESS</span>
                            </div>
                            <span className='font-bold'>{customer?.vendorAddresses[0]?.street}, {customer?.vendorAddresses[0]?.region}, {customer?.vendorAddresses[0]?.city}, {customer?.vendorAddresses[0]?.name}</span>
                        </div>
                    }
                    {customer?.vendorVatNo && 
                        <div className='border flex-1 flex items-center justify-between gap-3 p-2'>
                            <div className='flex flex-col text-right'>
                                <span className='text-xs'>الرقم الضريبي للمورد</span>
                                <span>Vendor VAT NO</span>
                            </div>
                            <span className='font-bold'>{customer.customerVatNo}</span>
                        </div>}
                    {/* {salesmanName && <div>{t('invoice.Salesman')}: {salesmanName}</div>} */}
                </div>
                <div  className='flex justify-between items-center p-3 bg-slate-10 font-bold border-t border-r border-l rounded-t'>
                    <div className='flex-1 text-center  flex flex-col'>
                        <span>اسم المنتج - الخدمة</span>
                        <span>PRODUCT / SERVICE NAME</span>
                    </div>
                    {
                    !isDelivery && 
                        <div className='w-20 text-center flex flex-col '>
                            <span>سعر الوحدة</span>
                            <span>PRICE UNIT</span>
                        </div>
                    }
                    <div className='w-16 text-center flex flex-col'>
                        <span>{t('invoice.quantity')}</span>
                        <span>QUANTITY</span>
                    </div>
                    {!isDelivery && <div className='w-24 text-center flex flex-col'>
                        <span>السعر</span>
                        <span>PRICE</span>
                        </div>}
                    {!isDelivery && <div className='w-24 text-center flex flex-col'>
                        <span>الخصم</span>
                        <span>PRICE</span>
                        </div>}
                    {!isDelivery && <div className='w-24 text-center flex flex-col'>
                        <span>إج. بعد الخصم </span>
                        <span>T.A DISCOUNT</span>
                        </div>}
                    {/* {!isDelivery && <div className='w-24 text-center  flex flex-col'>
                        <span>الضريبة ({SETTING_INV_VAT_PERCENT}%)</span>
                        <span>VAT ({SETTING_INV_VAT_PERCENT}%)</span>
                        </div>} */}
                    {!isDelivery && <div className='w-24 text-center flex flex-col'>
                        <span>إج. بعد الضريبة</span>
                        <span>T.A VAT</span>
                        </div>}
                </div>
                <div className='border '>{invProductsList}</div>
            </div>
        </>
    )
}

export default InvoiceBody