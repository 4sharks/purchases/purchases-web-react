import React from 'react'
import { Link } from 'react-router-dom';
import { useTenant } from '../../hooks';
// Icons
import { GrView } from 'react-icons/gr';
import { FiEdit } from 'react-icons/fi';
import { RiDeleteBinLine } from 'react-icons/ri';
import { MdOutlinePaid } from 'react-icons/md';
import { AiOutlinePrinter } from 'react-icons/ai';
import { CiDeliveryTruck } from 'react-icons/ci';
import { FaUserTie } from 'react-icons/fa';
import { PiUserListBold } from 'react-icons/pi';
import {  useTranslation } from 'react-i18next';

const InvListCard = (props) => {
    const [t] = useTranslation('global')
    const {tenantUsername} = useTenant();
    const {
        id,
        inv_no,
        net_amount,
        inv_date,
        is_paid,
        customer_name,
        salesman_name,
        inv_details,
        company_id,
        branch_id
    } = props.invoice ;
    const productList = inv_details.map((product,index) => (
        <li key={index} className='px-2 text-gray-500 flex justify-between items-center border-b border-slate-200'>
            <div className='flex-1' >{product.product_name}</div>
            <div className='w-6 '>{product.qty}</div>
            <div className=' w-16 '>{product.total_price} {props.SETTING_INV_CURRENCY}</div>
        </li>
    ))
    return (
            <div  className='flex flex-col  text-xs border rounded-lg '>
                <div className='flex  items-centers bg-slate-100 p-2  justify-between'>
                    <div className='font-bold'>{inv_no}</div>
                    <div className='' >{inv_date}</div>
                </div>
                <div className='flex  justify-between py-2 text-gray-800 bg-slate-50 '>
                    <div className='w-20  flex flex-col items-between justify-between '>
                        <div className='w-20 flex-1 flex flex-col justify-center items-center ' > <span className='font-bold'>{net_amount}</span> {props.SETTING_INV_CURRENCY}</div>
                        <div className={`w-20  text-center py-2 ${is_paid ? 'text-green-400': 'text-red-300'}`} >{is_paid ? 'Paid' : 'Not Paid'}</div>
                    </div>
                    <div className='flex-1 flex flex-col items-between px-2'>
                        <div className='flex justify-between'>
                            <div className='flex gap-1 items-center pb-1 '> <PiUserListBold/> {customer_name || t('invoice.GeneralCustomer')} </div>
                            <div className='flex gap-1 items-center pb-1' > <FaUserTie/> {salesman_name || t('invoice.NoSalesman') } </div>
                        </div>
                        <ul className='h-24 overflow-y-auto mt-1 ' > 
                            {/* {t('invoice.ProductsSumary')}: */}
                            <div className='rounded-lg p-1 bg-slate-100 my-1'>
                                {productList}
                            </div>
                        </ul>
                        <ul className='flex flex-1 justify-evenly pt-3   '>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><Link to={`/${tenantUsername}/invoices/show/${id}/${company_id}/${branch_id}`}><GrView /></Link></li>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><Link target='_blank' to={`/${tenantUsername}/invoices/print/${id}/${company_id}/${branch_id}`}><AiOutlinePrinter/></Link></li>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><Link target='_blank' to={`/${tenantUsername}/invoices/delivery/${id}/${company_id}/${branch_id}`}><CiDeliveryTruck/></Link></li>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><FiEdit/></li>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><RiDeleteBinLine/></li>
                            <li className=' transition-all duration-300  ease-out  hover:text-indigo-600 hover:text-lg h-4'><MdOutlinePaid/></li>
                        </ul>
                    </div>
                </div>
            </div>
    )
}

export default InvListCard