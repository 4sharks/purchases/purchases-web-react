import React from 'react'

const CardHeader = ({children}) => {
    return (
        <div 
            className='flex flex-col md:flex-row items-center justify-center text-xs text-slate-300 font-bold  bg-slate-50 p-2'>
                {children}
        </div>
    )
}

export default CardHeader