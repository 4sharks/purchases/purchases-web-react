import { useState,useEffect} from 'react'
import { useCurrent, useFetch, usePost } from '../../../hooks';
import {  MwButton, } from '../../ui';
import {langObjType,objLangTextHandler} from '../../../utils/global'
import CategoryForm from './CategoryForm';
import CardHeadeWithActions from '../../ui/CardHeadeWithActions';

const CreateCategory = ({setMode}) => {
   // let categoriesHandler = [];
    // public state
    const {currentLangList,currentTenantId,currentCompanyId,currentLangId} = useCurrent();
    const [formLang,setFormLang] = useState(currentLangId);
    const [errors,setErrors] = useState({});
    const [categories,setCategories] = useState([]);
    const categoryClasses = [
        {label: 'منتج' , value:'product'},
        {label: 'خدمة' , value:'service'},
    ]
    const {data:resultPost,loading:loadingPost,error:errorPost,postData} = usePost();
    const {data:dataFetchResponse,loading,error} = useFetch(`${process.env.REACT_APP_PRODUCTS_API_SERVER_BASE_URL}/categories/${currentTenantId}/${currentCompanyId}`);
    
    // Start Form state //////
    const [categoryParent,setCategoryParent] = useState('main');
    const [categoryClass,setCategoryClass] = useState();
    const [isActive,setIsActive] = useState(true);
    const [isShowOnWebsite,setIsShowOnWebsite] = useState(false);
    const [categoryName,setCategoryName] = useState(langObjType());
    const [categoryDesc,setCategoryDesc] = useState(langObjType());
    const [categoryMetaKeywords,setCategoryMetaKeywords] = useState(langObjType());
    const [categoryMetaDesc,setCategoryMetaDesc] = useState(langObjType());

    // TODO: Functions
    const getCategory = () =>{
        let cats = [];
        dataFetchResponse?.data.map((e) => {
            //const name = LangArrayToObjKeyValue(e.categoryName);
            e.categoryName.forEach(name => {
                cats.push( {label:name?.text,value:e._id,lang:name.lang})
            });
        });
        setCategories(cats);
    }

    const formValidate = () => {
        let errorObj = {};
        setErrors('');
        if(categoryParent==='sub'){
            errorObj = { ...errorObj, categoryParent : 'يجب اختيار التصنيف الفرعي' };
        }
        if(!categoryClass){
            errorObj = { ...errorObj, categoryClass : 'يجب اختيار فئة التصنيف ' };
        }
        if(!categoryName[currentLangId]){
            errorObj = { ...errorObj, [`categoryName.${currentLangId}`] : 'يجب ادخال اسم التصنيف'};
            setFormLang(currentLangId)
        }

        if(Object.keys(errorObj).length !== 0 ){
            setErrors(errorObj)
            return true
        }

        return false;
    }
    const submitHandler = (e) => {
        e.preventDefault();
        
        const _data = {
            categoryParent:categoryParent === 'main' ? null : categoryParent.value  ,
            categoryClass,
            categoryName: objLangTextHandler(categoryName),
            categoryDesc: objLangTextHandler(categoryDesc),
            categoryMetaKeywords: objLangTextHandler(categoryMetaKeywords),
            categoryMetaDesc: objLangTextHandler(categoryMetaDesc),
            isActive,
            isShowOnWebsite,
            tenantId:currentTenantId,
            companyId:currentCompanyId
        }
        
        if(formValidate()) return ;
        postData(`${process.env.REACT_APP_PRODUCTS_API_SERVER_BASE_URL}/categories`,_data);
        setMode('List');
    }
    // TODO: useEffect
    useEffect(() =>{
        if(dataFetchResponse){
            getCategory();
        }
    },[dataFetchResponse]);

    useEffect(() =>{
        if(resultPost){
            console.log(resultPost);
        }
    },[resultPost])

    
    return (
        <>
            <div className='flex flex-col gap-5 px-3 text-slate-500 text-sm' >
                <CardHeadeWithActions title={`اضف تصنيف جديد`}>
                        <MwButton type="cancelBtn" inGroup={true} onClick = {()=>setMode('List')} >ادارة التصنيفات</MwButton>
                        <MwButton type="saveBtn" onClick={submitHandler} inGroup={true} >حفظ</MwButton>
                </CardHeadeWithActions>
                <CategoryForm
                        categories = {categories}
                        currentLangList = {currentLangList}
                        formLang = {formLang}
                        setFormLang = {setFormLang}
                        errors = {errors}
                        categoryParent = {categoryParent}
                        setCategoryParent = {setCategoryParent}
                        categoryClasses = {categoryClasses}
                        setCategoryClass = {setCategoryClass}
                        categoryName = {categoryName}
                        setCategoryName = {setCategoryName}
                        categoryDesc = {categoryDesc}
                        setCategoryDesc = {setCategoryDesc}
                        categoryMetaKeywords = {categoryMetaKeywords}
                        setCategoryMetaKeywords = {setCategoryMetaKeywords}
                        categoryMetaDesc = {categoryMetaDesc}
                        setCategoryMetaDesc = {setCategoryMetaDesc}
                        isShowOnWebsite = {isShowOnWebsite}
                        setIsShowOnWebsite = {setIsShowOnWebsite}
                        isActive = {isActive}
                        setIsActive = {setIsActive}
                    />
            </div>
        </>
    )
}

export default CreateCategory