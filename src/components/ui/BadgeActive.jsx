import React from 'react'

const BadgeActive = ({state,label}) => {
    return (
        <div className={`text-center px-2 pb-1 rounded-lg   ${state ? ' bg-green-200 ' : 'bg-slate-300' }`}>{label}</div>
    )
}

export default BadgeActive