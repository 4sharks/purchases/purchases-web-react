import React from 'react'

const MwTabViewBody = ({children}) => {
    return (
        <div id='tab-body' className='w-full p-4 bg-slate-200 rounded-tl-xl rounded-b-xl'>{children}</div>
    )
}

export default MwTabViewBody