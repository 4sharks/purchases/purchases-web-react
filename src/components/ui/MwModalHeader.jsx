import React from 'react'

const MwModalHeader = (props) => {
    return (
            <div 
                className=' flex items-center justify-between  bg-slate-200 p-2 rounded-t  border-b border-slate-400 p-3 text-sm bg-slate-200  mb-2 sticky top-0'
                >
                {props.childern}
            </div>
        )
}

export default MwModalHeader