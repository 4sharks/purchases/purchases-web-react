import React from 'react'

const SalesmanSelectorItem = ({item,setSelectedItem,setOpen,setInputValue,inputValue}) => {
    return (
        <li 
            key={item._id}
            onClick={()=>{
                setSelectedItem({...item,label:item.fullname})
                setOpen(false);
                setInputValue("")
            }}
            className={`px-3 py-1 text-sm text-slate-600 hover:bg-sky-600 hover:text-white ${item?.fullname?.toLowerCase().includes(inputValue) ? 'block' : 'hidden'}`}
        >
            {item.fullname}
        </li>
    )
}

export default SalesmanSelectorItem