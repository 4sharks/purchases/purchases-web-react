import {  useState } from 'react';
import {  useTranslation } from 'react-i18next';
import  {MwSelectorProduct,MwInputText} from '../ui'
import InputQty from '../ui/InputQty';

const InvFormListItem = ({
    index,
    reloadProductList,
    showModalProductHandle,
    productList,
    setProductList,
    formErrors
    }) => {
    
    const [t] = useTranslation('global')
    const [pricelist,setPricelist]= useState({});    

    const onChangeProduct = (value) => {
        pricelist[`${value?._id}`] = value?.price;
        setPricelist({...pricelist});
        console.log('pricelist',pricelist);
        productList[index] = {
            index,
            productId:value?._id, 
            productName: value?.productName[0]?.text,
            qty:1,
            price:value?.price
        }
        setProductList([...productList]); 
        console.log(productList)
    };

    const incressQty = (index) => {
        if(!!productList[index] && parseInt(productList[index].qty) > 0){
            const newValue = parseInt(productList[index].qty)+ 1
            productList[index].qty = newValue;
            onChangeQty(newValue,index)
        }
        
    }
    const decressQty = (index) => {
        if(!!productList[index] && parseInt(productList[index].qty) > 1){
            const newValue = parseInt(productList[index].qty)- 1
            productList[index].qty = newValue;
            onChangeQty(newValue,index)
        }
        
    }
    const onChangeQty = (value,index) => {
        const priceOne = parseFloat( pricelist[`${productList[index].productId}`])  ;
        console.log('priceOne',priceOne);
        if(!(!!productList[index].qty)) {
            productList[index].qty=1
        }  
        if( !isNaN(productList[index].qty) && !isNaN(parseInt(productList[index].qty)) >= 1 ){
           // priceOne = (productList[index].price / productList[index].qty )
            productList[index].qty = value
            if((productList[index].qty * priceOne) &&  !isNaN(productList[index].qty * priceOne) ){
                productList[index].price  =  (productList[index].qty * priceOne).toFixed(2)
            }else{
                productList[index].price  = priceOne.toFixed(2)
            }
            console.log('productList.qty',productList[index].qty,'price;',productList[index].price)
        }
        setProductList([...productList]); 
        
    }

    const onChangePrice = () =>{

    }

    

    return (
        <div className='flex border-b items-center  '>
            <div className="flex flex-1 flex-col">
                <MwSelectorProduct 
                    invalid = {!!formErrors?.productList}
                    // withLabel={index === 0 && true}
                    reloadProductList={reloadProductList} 
                    onCreateProduct={showModalProductHandle} 
                    onChangeCallback ={onChangeProduct} />
            </div>
            <div className="flex flex-col w-24 mx-1 pt-1">
                <InputQty 
                    // label={`${index === 0 ? t('invoice.quantity') : ''}`}
                    id='qty' 
                   // disabled = {!(!!productList[index]?.productId)}
                    value={productList[index]?.qty || ''} 
                    onChange={(e)=>onChangeQty(e.target.value,index)}
                    onIncress={(e)=>incressQty(index)}
                    onDecress={(e)=>decressQty(index)}
                    // disabled={true}
                    />
            </div>
            <div className="flex flex-col w-20 mx-1 pt-1  ">
                <MwInputText 
                    // label={`${index === 0 ? t('invoice.price')  : ''}`}
                    id='price' 
                    disabled={true}
                    value={productList[index]?.price || ''} 
                    
                    onChange={(e)=>onChangePrice(e.target.value,index)} />
            </div>
        </div>
    )
}

export default InvFormListItem