import React from 'react'
import { Link } from 'react-router-dom';
import {MdOutlineAddBox} from 'react-icons/md'
import InvListCard from './InvListCard'
import MwButton from '../ui/MwButton';
import { useTenant,useInvoice,useCurrent } from '../../hooks/';
import {  useTranslation } from 'react-i18next';


const InvList = (props) => {
    const [t] = useTranslation('global')
    const {tenantUsername} = useTenant();
    const {currentLangId} = useCurrent();
    const {SETTING_INVOICE} = useInvoice();
    const INV_CURRENCY = SETTING_INVOICE?.INV_CURRENCY
    const SETTING_INV_CURRENCY = INV_CURRENCY && JSON.parse(INV_CURRENCY).filter(currency => currency.lang === currentLangId )[0]?.code || 'SAR'
    const {invoices} = props;
    const invList = invoices?.data?.map(inv => (<InvListCard key={inv.id} invoice={inv} SETTING_INV_CURRENCY={SETTING_INV_CURRENCY} />))
    return (
        <div className='md:ps-2 '>
            <Link to={`/${tenantUsername}/invoices/create`} >
                <MwButton inGroup = {true} size='sm'
                            type = 'saveBtn'><MdOutlineAddBox size={16}/>{t('pagesTitles.createInvoice')}</MwButton>
            </Link>
            <div className='grid grid-cols-1  md:grid-cols-2 gap-3 my-3 items-center justify-center '>{invList}</div>
        </div>
        
    )
}

export default InvList